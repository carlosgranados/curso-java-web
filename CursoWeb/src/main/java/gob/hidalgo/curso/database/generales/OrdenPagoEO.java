package gob.hidalgo.curso.database.generales;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.validation.constraints.NotBlank;
import org.apache.ibatis.type.Alias;

import gob.hidalgo.curso.database.dominios.EstatusOrdenPagoDO;
import gob.hidalgo.curso.utils.EntityObject;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Alias("OrdenPagoEO")
public class OrdenPagoEO extends EntityObject implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String folio;
	private LocalDateTime fechaCreacion;
	private LocalDateTime fechaPago;
	private LocalDateTime fechaCancelacion;
	private BigDecimal montoTotal;
	private String motivoCancelacion;
	private EstatusOrdenPagoDO estatus;
	
	public OrdenPagoEO() {
		super();
		montoTotal = BigDecimal.ZERO;
		estatus = EstatusOrdenPagoDO.PENDIENTE;
	}
}
